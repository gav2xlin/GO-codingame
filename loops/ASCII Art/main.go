package main

import "fmt"
import "os"
import "bufio"
import "strings"

func main() {
    scanner := bufio.NewScanner(os.Stdin)
    scanner.Buffer(make([]byte, 1000000), 1000000)

    var L int
    scanner.Scan()
    fmt.Sscan(scanner.Text(),&L)
    
    var H int
    scanner.Scan()
    fmt.Sscan(scanner.Text(),&H)
    
    scanner.Scan()
    T := strings.ToUpper(scanner.Text())

    pos := make([]int, 0)
    for _, ch := range T {
        shift := 26
        if 'A' <= ch && ch <= 'Z' {
            shift = int(ch - 'A')
        }
        pos = append(pos, shift * L)
    }

    for i := 0; i < H; i++ {
        scanner.Scan()
        ROW := scanner.Text()

        for j := range T {
            for k := 0; k < L; k++ {
                fmt.Print(string(ROW[pos[j] + k]))
            }
        }
        fmt.Println()
    }
}